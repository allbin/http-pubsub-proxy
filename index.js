let express = require('express');
let cors = require('cors');
let {PubSub} = require('@google-cloud/pubsub');
let config = require('./config');

let app = new express();
let clients = {};

function cleanClients() {
    let now = Date.now();
    Object.keys(clients).forEach((token) => {
        if ((now - clients[token].last_seen) > 2 * 60 * 1000) {
            console.log("cleared messages for " + token);
            delete clients[token];
        }
    });
    setTimeout(cleanClients, 60 * 1000);
}
setTimeout(cleanClients, 60 * 1000);


if (!config.accepted_secrets || config.accepted_secrets.length === 0) {
    console.warn("No accepted secrets configured");
}

app.use(cors({
    origins: ['*']
}));

function authAndAddClient(req, res, next) {
    if (!req.headers.authorization) {
        let err = new Error("No authorization header present");
        err.status = 401;
        return next(err);
    }
    let parts = req.headers.authorization.split(' ');
    if (parts[0].toLowerCase() !== 'bearer') {
        let err = new Error("Unsupported authorization scheme");
        err.status = 401;
        return next(err);
    }

    if (config.accepted_secrets.indexOf(parts[1].toLowerCase()) === -1) {
        console.log('unauthorized token: ' + parts[1]);
        let err = new Error("Unauthorized");
        err.status = 401;
        return next(err);
    }

    let now = Date.now();

    req.token = parts[1];
    if (!clients.hasOwnProperty(req.token)) {
        clients[req.token] = { last_seen: now, messages: [] };
    } else {
        clients[req.token].last_seen = now;
    }

    return next();
}

app.use(express.json());

let pubsub = new PubSub({
    keyFilename: config.google.service_account_file
});
let sub = pubsub.subscription(`projects/${config.google.project_id}/subscriptions/commands`);
sub.on('message', (msg) => {
    msg.data = JSON.parse(Buffer.from(msg.data).toString('utf8'));
    console.log("incoming message", JSON.stringify(msg.attributes), JSON.stringify(msg.data));
    Object.keys(clients).forEach((token) => {
        clients[token].messages.push(msg);
    });
    msg.ack();
});

let url_prefix = '/api/v1';

app.get(url_prefix + '/receive', authAndAddClient, (req, res) => {
    if (clients.hasOwnProperty(req.token) === false) {
        return res.json([]);
    }

    return res.json(clients[req.token].messages.splice(0));
});


function postMessage(topic_name, attributes, msg) {
    return pubsub
        .topic(topic_name)
        .publisher()
        .publish(Buffer.from(msg), attributes);
}

app.post(url_prefix + '/send/:eui', authAndAddClient, (req, res, next) => {
    console.log("outgoing message", JSON.stringify(req.body));
    let msg = JSON.stringify(req.body);
    let topic_name = `projects/${config.google.project_id}/topics/events`;
    let attributes = {
        subFolder: 'guards/' + req.params.eui,
        deviceId: 'some-device'
    };
    postMessage(topic_name, attributes, msg)
        .then(() => {
            console.log("posted...");
            return res.json({});
        })
        .catch((err) => {
            console.log(err.stack);
            return next(err);
        });
});

app.post(url_prefix + '/cmd/:eui', (req, res, next) => {
    console.log("command", JSON.stringify(req.body));
    let topic_name = `projects/${config.google.project_id}/topics/commands`;
    let attributes = {
        subFolder: 'guards/' + req.params.eui,
        deviceId: 'some-device'
    };
    let msg = JSON.stringify(req.body);
    postMessage(topic_name, attributes, msg)
        .then(() => {
            console.log("posted...");
            return res.json({});
        })
        .catch((err) => {
            console.log(err.stack);
            return next(err);
        });
});

app.listen(50000, () => {
    console.log("running on port 50000...");
});


