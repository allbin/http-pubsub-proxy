let fs = require('fs');
let exec = require('child_process').exec;

let gulp = require('gulp');
let del = require('del');
let bump = require('gulp-bump');
let eslint = require('gulp-eslint');

let pkg = JSON.parse(fs.readFileSync('package.json'));
let docker_image_name = 'docker.allbin.se/release/' + pkg.name;

let paths = {
    static: ['package.json', 'Dockerfile'],
    scripts: ['**/*.js', '!node_modules/**', '!build/**', '!dist/**', '!Gulpfile.js']
};


let execPromise = (cmd) => {
    return new Promise((resolve, reject) => {
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                return reject(new Error(cmd + ": " + stderr));
            }
            return resolve();
        });
    });
};

// ---- preparations ----
gulp.task('clean', () => {
    return del(['build', 'dist']);
});

gulp.task('lint', () => {
    return gulp.src(paths.scripts)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});


// ---- build ----
gulp.task('build:prep', ['clean', 'lint']);

gulp.task('build:scripts', ['build:prep'], () => {
    return gulp.src(paths.scripts)
        .pipe(gulp.dest('dist'));
});

gulp.task('build:static', ['build:prep'], () => {
    return gulp.src(paths.static)
        .pipe(gulp.dest('dist'));
});

gulp.task('build', ['build:static', 'build:scripts']);


// ---- docker ----
gulp.task('docker:build', ['build'], (cb) => {
    exec('docker build -t ' + docker_image_name + ' .', (err, stdout, stderr) => {
        return cb(err);
    });
});

gulp.task('docker:tag', ['docker:build'], (cb) => {
    let latest_tag = docker_image_name;
    let version_tag = docker_image_name + ':' + pkg.version;
    exec('docker tag ' + latest_tag + ' ' + version_tag, (err, stdout, stderr) => {
        return cb(err);
    });
});

gulp.task('docker:push', ['docker:tag'], (cb) => {
    let latest_tag = docker_image_name;
    let version_tag = docker_image_name + ':' + pkg.version;

    exec('docker push ' + latest_tag, (err, stdout, stderr) => {
        if (err) {
            return cb(err);
        }
        exec('docker push ' + version_tag, (err, stdout, stderr) => {
            return cb(err);
        });
    });
});

gulp.task('bump', ["build"], (cb) => {
    return gulp.src('./package.json')
        .pipe(bump())
        .pipe(gulp.dest('.'));
});
gulp.task('bump:minor', ["build"], (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'minor'}))
        .pipe(gulp.dest('.'));
});
gulp.task('bump:major', ["build"], (cb) => {
    return gulp.src('./package.json')
        .pipe(bump({type: 'major'}))
        .pipe(gulp.dest('.'));
});

function pushBumpAndTag(cb) {
    try {
        let pkg = JSON.parse(fs.readFileSync('package.json'));

        Promise.resolve()
            .then(() => { return execPromise('git add package.json'); })
            .then(() => { return execPromise('git commit -m "Release v' + pkg.version + '"'); })
            .then(() => { return execPromise('git tag v' + pkg.version); })
            .then(() => { return execPromise('git push && git push --tags'); })
            .then(() => { return cb(); });

    } catch (err) {
        return cb(err);
    }
}

gulp.task('release', ['bump'], (cb) => {
    pushBumpAndTag(cb);
});
gulp.task('release:minor', ['bump:minor'], (cb) => {
    pushBumpAndTag(cb);
});
gulp.task('release:major', ['bump:major'], (cb) => {
    pushBumpAndTag(cb);
});


gulp.task('default', ['build']);
gulp.task('docker', ['docker:push']);
