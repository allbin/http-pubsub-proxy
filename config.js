let default_config = require('./default_config.js');
let fs = require('fs');

let conf = {};
try {
    let contents = fs.readFileSync('/config.json');
    conf = JSON.parse(contents);
} catch (e) {
    /* nop */
    console.warn("WARNING: no custom config.json found");
}

module.exports = Object.assign({}, default_config, conf);
