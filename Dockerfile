FROM node:alpine
RUN mkdir /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY . .
RUN apk update && apk add git
RUN npm install && npm run build

FROM node:alpine
WORKDIR /app
COPY --from=0 /app/dist .
RUN apk update && apk add git && npm install --silent --production && npm cache clean --force
CMD ["npm", "start"]

